const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const User = require("../models/User");

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id);
  done(null, user);
});

//Registra y valida si existe un usuario mediante email
passport.use(
  "local-up",
  new localStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      const user = await User.findOne({ email: email });

      console.log(user, "--------> IS IN THE DATABASE");
      if (user) {
        return done(
          null,
          false,
          req.flash('signupMessage', 'The email is already in the database.')
        );
      } else {
        const newUser = new User();
        newUser.email = email;
        newUser.password = newUser.encrypPassword(password);
        console.log(newUser);
        await newUser.save();
        done(null, newUser);
      }
    }
  )
);


passport.use('local-in', new localStrategy({
  usernameField: "email",
  passwordField: "password",
  passReqToCallback: true
},
async (req, email, password, done) => {

  const user = /*ATENCIÓN A LOS AWAIT!!*/ await User.findOne({email: email});

  if(!user){
    return done(null, false, req.flash('loginMessage', `User doesn´t exist! SIGN IN`));
  }

  if(!user.comparePassword(password)){
    return done(null, false, req.flash('loginMessage', 'Password incorrect!'));
  }

  return done(null, user);


}

))
