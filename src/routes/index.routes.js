const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../models/User')

//INDEX ULR
router.get('/', (req, res, next)=>{
    res.status(200).render("index");
});

//SIGN UP ROUTE
router.get('/signup', (req, res, next) =>{
    if(!req.isAuthenticated()) {
      return res.status(202).render('up');
      
      }
      return res.status(200).redirect('/profile');
      
});

router.post('/signup', passport.authenticate('local-up', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    passReqToCallback: true
}));

//SIGN IN ROUTE
router.post('/login', passport.authenticate('local-in', {
    successRedirect: '/profile',
    failureRedirect: '/login',
    passReqToCallback: true
}));



router.get('/login', (req, res, next) =>{
    if(!req.isAuthenticated()) {
      
      return res.status(403).render('login');
     
      }
       
      return res.status(202).redirect('/profile');
     


});


//AQUI EMPEIZAN LAS RUTAS PROTEGIDAS POR VVALIDATEUSER()
router.use((req, res, next)=>{
ValidateUser(req,res,next);
next();
})


//SIGN OUT ROUTE
router.get('/close-session', (req, res, next) =>{
req.logout();
res.status(202).redirect('/');

return
});

router.get('/profile', (req, res, next)=>{
  return res.status(202).render('profile');

});

function ValidateUser(req, res, next) {
  if(req.isAuthenticated()) {
    return next();
  }

  return res.status(403).redirect('/login')
}

module.exports = router