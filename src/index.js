//EXPRESS
const express = require('express');
const path = require("path");

//CONSOLELOG PETITIONS
const morgan = require('morgan')

//VIEWS
const engine = require('ejs-mate')

//AUTH
const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash')


//INITIALS
const app = express();
const PORT = 3050;
require('./db');
require('./auth/local-auth');


//RENDER VIEWS 
app.set('views', `${__dirname}/view` );
app.engine('ejs', engine);
app.set('view engine', 'ejs');


//STATICS
app.use(express.static(path.join(__dirname, "public")))

//MIDDLEWARES

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}))
app.use(session({
secret: 'loginsecret',
resave: false,
saveUninitialized: false,
cookie: {
   maxAge: 3600000
 },
}))
app.use(passport.initialize());
app.use(passport.session());

//Mensajes a través de diferentes peticiones
app.use(flash());


//MENSAJES FLASH
app.use((req, res, next)=>{
   app.locals.signupMessage = req.flash('signupMessage');
   app.locals.loginMessage = req.flash('loginMessage');
   app.locals.user = req.user;
   next();
})


//ROUTES
const indexRoutes = require('./routes/index.routes');
const { urlencoded, json } = require('express');
app.use('/', indexRoutes);



//Start Server
app.listen(PORT, (req, res, next)=>{
console.log(`server in http://localhost:${PORT}`);
})
