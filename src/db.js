const mongoose = require("mongoose");
const { mongodb } = require("./keys");

mongoose.connect(mongodb.URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(console.log("MONGODB WORKING!"))
.catch ( err => console.error(err));